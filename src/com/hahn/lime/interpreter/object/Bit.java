package com.hahn.lime.interpreter.object;

public class Bit implements Evaluated {
	private int val;
	
	/**
	 * Creates a bit equal to the least significant bit of the given value
	 * @param v The value to take the least significate bit of
	 */
	private Bit(int v) {
		this.val = (v & 0x1);
	}
	
	@Override
	public int bitValue() {
		return val;
	}
	
	@Override
	public String toString() {
		return "$" + String.valueOf(bitValue());
	}
	
	public static Bit valueOf(Integer valueOf) {
		return ((valueOf & 0x1) == 0 ? ZERO : ONE);
	}
	
	private static final Bit ZERO = new Bit(0), ONE = new Bit(1);

}
