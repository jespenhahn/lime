package com.hahn.lime.interpreter.object;

import com.hahn.lime.exceptions.MismatchArgumentException;
import com.hahn.lime.interpreter.Interpreter;
import com.hahn.lime.parser.expressions.Expression;

public class Matrix implements Evaluated {
	private Evaluated[] vals;
	
	public Matrix(Evaluated[] vals) {
		this.vals = vals;
	}
	
	public int size() {
		return vals.length;
	}
	
	public Evaluated getVal(int idx) {
		return vals[idx];
	}
	
	public void setBitValues(Expression e, Interpreter in) {
		Evaluated input = e.evaluate(in);
		if (input instanceof Matrix) {
			Matrix inputMatrix = (Matrix) input;
			for (int i = 0; i < size() && i < inputMatrix.size(); i++) {
				Evaluated targVal = getVal(i);
				if (targVal instanceof BitVar) {
					Evaluated inputVal = inputMatrix.getVal(i);
					((BitVar) targVal).setVal(inputVal);
				} else {
					throw new MismatchArgumentException("Cannot do matrix assignment to a non-variable object", e);
				}
			}
		} else {
			throw new MismatchArgumentException("Cannot set a matrix based on a non-matrix", e);
		}
	}
	
	@Override
	public int bitValue() {
		throw new IllegalArgumentException("Cannot convert a matrix to an integer");
	}
	
	@Override
	public String toString() {
		return vals.toString();
	}

}
