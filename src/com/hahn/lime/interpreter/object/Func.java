package com.hahn.lime.interpreter.object;

import com.hahn.lime.exceptions.UndefinedFunctionException;
import com.hahn.lime.interpreter.Frame;
import com.hahn.lime.interpreter.Interpreter;
import com.hahn.lime.parser.expressions.Expression;
import com.hahn.lime.parser.expressions.NameExpression;

public class Func implements Evaluated {
	private String name;
	private String[] paramNames;
	private Expression body;
	
	public Func(String name, String[] params) {
		this.name = name;
		this.paramNames = params;
	}
	
	public void setBody(Expression body) {
		this.body = body;
	}

	public Evaluated call(NameExpression name, Expression[] params, Interpreter in) {
		if (body == null) throw new UndefinedFunctionException("Tried to call the function \"" + name + "\" with " + params.length + " parameters before it's body was defined", name);
		
		Frame f = new Frame();
		for (int i = 0; i < paramNames.length; i++) f.defVar(paramNames[i], params[i].evaluate(in));
		
		in.pushFrame(f);
		Evaluated res = body.evaluate(in);
		in.popFrame();
		
		return res;
	}

	@Override
	public int bitValue() {
		throw new IllegalArgumentException("Cannot convert a function \"" + name + "\" to an integer");
	}
	
	@Override
	public String toString() {
		if (body != null) return body.toString();
		else return name + "$" + paramNames.length;
	}
	
}
