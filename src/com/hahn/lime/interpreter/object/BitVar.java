package com.hahn.lime.interpreter.object;

import com.hahn.lime.interpreter.Interpreter;
import com.hahn.lime.parser.expressions.Expression;

public class BitVar implements Evaluated {
	private int val;
	
	/**
	 * Create a new var with a value equal to the least significant bit of the given integer
	 * @param val The bit value of this var will be equal to the least significant bit of this integer
	 */
	public BitVar(int val) {
		this.val = (val & 0x1);
	}
	
	/**
	 * Set the bit of this variable equal to the bit value of the given evaluated object
	 * @param v The evaluated object to get the bit from (uses least significant bit)
	 */
	public void setVal(Expression e, Interpreter in) {
		setVal(e.evaluate(in));
	}
	
	public void setVal(Evaluated e) {
		this.val = (e.bitValue() & 0x1);
	}

	@Override
	public int bitValue() {
		return val;
	}
	
	@Override
	public String toString() {
		return String.valueOf(bitValue());
	}
}
