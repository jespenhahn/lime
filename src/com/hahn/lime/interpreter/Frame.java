package com.hahn.lime.interpreter;

import java.util.HashMap;
import java.util.Map;

import com.hahn.lime.interpreter.object.BitVar;
import com.hahn.lime.interpreter.object.Evaluated;
import com.hahn.lime.parser.expressions.NameExpression;

public class Frame {
	private Map<String, BitVar> vars;
	
	public Frame() {
		vars = new HashMap<String, BitVar>();
	}
	
	public void defVar(String key, Evaluated val) {
		if (val instanceof BitVar) vars.put(key, (BitVar) val);
		else vars.put(key, new BitVar(val.bitValue()));
	}
	
	public BitVar getVar(NameExpression name) {
		return getVar(name.getName());
	}

	public BitVar getVar(String key) {
		return vars.get(key);		
	}
}
