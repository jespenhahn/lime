package com.hahn.lime.interpreter.custom;

import com.hahn.lime.interpreter.Interpreter;
import com.hahn.lime.interpreter.object.Evaluated;
import com.hahn.lime.parser.expressions.Expression;

public interface PrefixOperatorInterpreter {

	Evaluated evaluate(Interpreter in, Expression left);

}
