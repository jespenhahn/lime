package com.hahn.lime.interpreter.custom;

import com.hahn.lime.interpreter.Interpreter;
import com.hahn.lime.interpreter.object.Bit;
import com.hahn.lime.interpreter.object.Evaluated;
import com.hahn.lime.parser.expressions.Expression;

public class PrefixNotInterpreter implements PrefixOperatorInterpreter {

	@Override
	public Evaluated evaluate(Interpreter in, Expression left) {
		return Bit.valueOf(~(left.evaluate(in).bitValue()));
	}

}
