package com.hahn.lime.interpreter.custom;

import com.hahn.lime.exceptions.MismatchArgumentException;
import com.hahn.lime.interpreter.Interpreter;
import com.hahn.lime.interpreter.object.BitVar;
import com.hahn.lime.interpreter.object.Evaluated;
import com.hahn.lime.interpreter.object.Func;
import com.hahn.lime.interpreter.object.Matrix;
import com.hahn.lime.parser.expressions.Expression;

public class AssignInterpreter implements InfixOperatorInterpreter {

	@Override
	public Evaluated evaluate(Interpreter in, Expression left, Expression right) {
		if (right == null) throw new MismatchArgumentException("Attempt to assign null value", left);
		
		Evaluated eLeft = left.define(in);		
		if (eLeft instanceof Func) ((Func) eLeft).setBody(right);
		else if (eLeft instanceof BitVar) ((BitVar) eLeft).setVal(right, in);
		else if (eLeft instanceof Matrix) ((Matrix) eLeft).setBitValues(right, in);
		else throw new MismatchArgumentException("Illegal left-hand argument \"" + eLeft + "\"", left);
		
		return eLeft;
	}
	
}
