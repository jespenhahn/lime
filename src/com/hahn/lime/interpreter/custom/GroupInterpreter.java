package com.hahn.lime.interpreter.custom;

import com.hahn.lime.interpreter.Interpreter;
import com.hahn.lime.interpreter.object.Evaluated;
import com.hahn.lime.parser.expressions.Expression;

public class GroupInterpreter implements PrefixOperatorInterpreter {

	@Override
	public Evaluated evaluate(Interpreter in, Expression left) {
		return left.evaluate(in);
	}

}
