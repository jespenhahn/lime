package com.hahn.lime.interpreter.custom;

import com.hahn.lime.interpreter.Interpreter;
import com.hahn.lime.interpreter.object.Bit;
import com.hahn.lime.interpreter.object.Evaluated;
import com.hahn.lime.parser.expressions.Expression;

public class InfixAndInterpreter implements InfixOperatorInterpreter {

	@Override
	public Evaluated evaluate(Interpreter in, Expression left, Expression right) {
		return Bit.valueOf(left.evaluate(in).bitValue() & right.evaluate(in).bitValue());
	}

}
