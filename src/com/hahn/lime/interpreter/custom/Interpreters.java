package com.hahn.lime.interpreter.custom;

import java.util.HashMap;
import java.util.Map;

import com.hahn.lime.exceptions.DuplicateInfixDefinitionException;
import com.hahn.lime.exceptions.DuplicatePrefixDefinitionException;
import com.hahn.lime.exceptions.UndefinedInfixException;
import com.hahn.lime.exceptions.UndefinedPrefixException;
import com.hahn.lime.interpreter.Interpreter;
import com.hahn.lime.interpreter.object.Evaluated;
import com.hahn.lime.lexer.EnumToken;
import com.hahn.lime.lexer.Token;
import com.hahn.lime.parser.expressions.Expression;

public class Interpreters {

	private static final Map<String, InfixOperatorInterpreter> mInfixOPInterpreter = new HashMap<String, InfixOperatorInterpreter>();
	private static final Map<String, PrefixOperatorInterpreter> mPrefixOPInterpreter = new HashMap<String, PrefixOperatorInterpreter>();
	
	public static void registerInfix(Token name, InfixOperatorInterpreter in) {
		String key = name.getVal();
		if (mInfixOPInterpreter.containsKey(key)) throw new DuplicateInfixDefinitionException(name);
		
		registerInfix(key, in);
	}
	
	public static void registerInfix(EnumToken token, InfixOperatorInterpreter in) {
		registerInfix(token.str, in);
	}
	
	private static void registerInfix(String key, InfixOperatorInterpreter in) {
		mInfixOPInterpreter.put(key, in);
	}
	
	public static Evaluated evaluateInfix(Interpreter in, Token name, Expression left, Expression right) {
		InfixOperatorInterpreter iin = mInfixOPInterpreter.get(name.getVal());
		if (iin == null) throw new UndefinedInfixException(name);
		
		return iin.evaluate(in, left, right);
	}
	
	public static void registerPrefix(Token name, PrefixOperatorInterpreter in) {
		String key = name.getVal();
		if (mPrefixOPInterpreter.containsKey(key)) throw new DuplicatePrefixDefinitionException(name);
		
		registerPrefix(key, in);
	}
	
	public static void registerPrefix(EnumToken token, PrefixOperatorInterpreter in) {
		registerPrefix(token.str, in);
	}
	
	private static void registerPrefix(String key, PrefixOperatorInterpreter in) {
		mPrefixOPInterpreter.put(key, in);
	}
	
	public static Evaluated evaluatePrefix(Interpreter in, Token name, Expression left) {
		PrefixOperatorInterpreter pin = mPrefixOPInterpreter.get(name.getVal());
		if (pin == null) throw new UndefinedPrefixException(name);
		
		return pin.evaluate(in, left);
	}
}
