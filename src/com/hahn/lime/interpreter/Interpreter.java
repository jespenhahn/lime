package com.hahn.lime.interpreter;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import com.hahn.lime.exceptions.DuplicateFunctionDefinitionException;
import com.hahn.lime.exceptions.ParseException;
import com.hahn.lime.exceptions.UndefinedFunctionException;
import com.hahn.lime.exceptions.UndefinedVariableException;
import com.hahn.lime.interpreter.object.BitVar;
import com.hahn.lime.interpreter.object.Func;
import com.hahn.lime.lexer.EnumToken;
import com.hahn.lime.lexer.Lexer;
import com.hahn.lime.lexer.Locatable;
import com.hahn.lime.lexer.Token;
import com.hahn.lime.parser.Parser;
import com.hahn.lime.parser.expressions.Expression;
import com.hahn.lime.parser.expressions.NameExpression;
import com.hahn.lime.parser.expressions.sysfunc.DebugExpression;
import com.hahn.lime.parser.expressions.sysfunc.ImportFunc;

public class Interpreter {
	public static final Locatable START = new Token(EnumToken.identifier, "", -1, -1);
	
	private Stack<Frame> frames;
	
	private Map<String, Func> funcs;
	private Map<String, BitVar> vars;
	
	public Interpreter() {
		this.frames = new Stack<Frame>();
		
		this.funcs = new HashMap<String, Func>();
		this.vars = new HashMap<String, BitVar>();
		
		defineFuncs();
	}
	
	public Stack<Expression> interpret(InputStream is) throws IOException {
		Lexer lex = new Lexer(is);
		Parser p = new Parser(lex);
		
		// final long start = System.currentTimeMillis();
		
		Stack<Expression> es = new Stack<Expression>();
		while (lex.hasNext()) {
			Expression e = p.parseExpression(0, true);
			if (lex.hasNext()) {
				Token next = lex.next();
				if(next.getEnum() != EnumToken.semicolon) {
					throw new ParseException("Unexpected end of expression or missing \";\"", next);
				}
			}
			
			if (e != null) {
				e.evaluate(this);
				es.push(e);
			}
		}
		
		// System.out.println("Done in " + (System.currentTimeMillis() - start) + "ms");
		
		lex.close();
		
		return es;
	}
	
	private void defineFuncs() {
		defFunc("debug", new String[] { "mss" }).setBody(new DebugExpression(START));
		defFunc("import", 1, new ImportFunc());
	}
	
	public Func defFunc(NameExpression name, String[] params) {
		String id = getFuncId(name, params.length);
		if (funcs.containsKey(id)) throw new DuplicateFunctionDefinitionException("A function \"" + name + "\" with " + params.length + " parameters has already been defined", name);
		
		return defFunc(name.getName(), params);
	}
	
	private Func defFunc(String name, String[] params) {
		return defFunc(name, params.length, new Func(name, params));
	}
	
	private Func defFunc(String name, int numParams, Func f) {
		funcs.put(getFuncId(name, numParams), f);
		return f;
	}

	public Func getFunc(NameExpression name, int numParams) {
		Func f = funcs.get(getFuncId(name, numParams));
		if (f == null) throw new UndefinedFunctionException("Tried to call the function \"" + name + "\" with " + numParams + " parameters before it was defined", name);
		
		return f;
	}

	private String getFuncId(NameExpression name, int numParams) {
		return getFuncId(name.getName(), numParams);
	}
	
	private String getFuncId(String name, int numParams) {
		return name + "$" + numParams;
	}

	public BitVar defVar(NameExpression name) {
		BitVar v = getFrameVar(name);
		if (v == null) {			
			v = new BitVar(0);
			setFrameVar(name, v);
		}
		
		return v;
	}
	
	public BitVar getVar(NameExpression name) {
		BitVar v = getFrameVar(name);
		if (v == null) throw new UndefinedVariableException("Tried to use \"" + name.getName() + "\" before it was defined", name);
		
		return v;
	}
	
	public void pushFrame(Frame f) {
		frames.push(f);
	}
	
	public void popFrame() {
		frames.pop();
	}
	
	private BitVar getFrameVar(NameExpression name) {
		if (!frames.empty()) { 
			BitVar local = frames.peek().getVar(name);
			if (local != null) return local;
		}
		
		return vars.get(name.getName());
	}
	
	private void setFrameVar(NameExpression name, BitVar var) {
		if (!frames.empty()) frames.peek().defVar(name.getName(), var);
		else vars.put(name.getName(), var);
	}
	
	public void setLocalVar(String key, Expression val) {
		if (!frames.empty()) frames.peek().defVar(key, val.evaluate(this));
		else throw new RuntimeException("Can't set a local variable on the global frame");
	}
	
	public BitVar getLocalVar(String key) {
		if (!frames.empty()) return frames.peek().getVar(key);
		else throw new RuntimeException("Can't get a local variable on the global frame");
	}
}
