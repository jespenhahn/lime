package com.hahn.lime.lexer;

public interface Locatable {

	int getRow();
	int getCol();
	
}
