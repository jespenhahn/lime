package com.hahn.lime.lexer;

import java.util.ArrayList;
import java.util.List;

public enum EnumToken {
	comment, whitespace, 
	
	bit("$", Group.OTHER), 
	identifier("$$$", Group.OTHER),
	
	and("&", Group.OPERATOR),
	bor("|", Group.OPERATOR),
	not("!", Group.OPERATOR),
	xor("^", Group.OPERATOR),
	equ("=", Group.OPERATOR),
	
	open_p ("(", Group.SEPERATOR),
	close_p(")", Group.SEPERATOR),
	open_s ("[", Group.SEPERATOR),
	close_s("]", Group.SEPERATOR),
	comma  (",", Group.SEPERATOR),
	semicolon (";", Group.SEPERATOR);
	
	public final String str;
	private EnumToken(String str, List<EnumToken> group) {
		this.str = str;
		group.add(this);
	}
	
	private EnumToken() {
		this("", Group.OTHER);
	}
	
	public static EnumToken valueOf(String str, List<EnumToken> group) {
		for (EnumToken t: group) {
			if (t.str.equals(str)) {
				return t;
			}
		}
		
		return null;
	}
	
	public static class Group {
		public static final List<EnumToken> OPERATOR  = new ArrayList<EnumToken>(), 
											SEPERATOR = new ArrayList<EnumToken>(), 
											OTHER     = new ArrayList<EnumToken>();
	}
}
