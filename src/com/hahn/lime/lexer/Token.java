package com.hahn.lime.lexer;

public class Token implements Locatable {
	private EnumToken token;
	private String val;
	private int row, col;
	
	public Token(EnumToken t, String val, int row, int col) {
		this.token = t;
		this.val = val;
		this.row = row;
		this.col = col;
	}
	
	public String getVal() {
		return val;
	}
	
	public EnumToken getEnum() {
		return token;
	}
	
	@Override
	public int getRow() {
		return row;
	}
	
	@Override
	public int getCol() {
		return col;
	}
	
	@Override
	public String toString() {
		return token.toString();
	}
}
