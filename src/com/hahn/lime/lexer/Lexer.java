package com.hahn.lime.lexer;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

public class Lexer implements Iterator<Token> {
	private InputStreamReader stream;
	
	private int tokenIdx;
	private Stack<Token> tokens;
	private Stack<Token> printTokens;
	
	private StringBuilder tokenBuilder;
	private int tokenRow, tokenCol, row, col;
	private char c;
	
	public Lexer(InputStream s) throws IOException {
		stream = new InputStreamReader(s, Charset.forName("UTF-8"));
		
		tokenIdx = 0;
		tokens = new Stack<Token>();
		printTokens = new Stack<Token>();
		
		tokenBuilder = new StringBuilder(128);
		tokenRow = tokenCol = 0;
		
		// load first character and force position to 0,0
		read();
		row = col = 0;
	}
	
	public void close() throws IOException {
		stream.close();
	}
	
	public List<Token> getAllTokens() {
		return tokens;
	}
	
	/**
	 * Append the first character to the token being built
	 */
	private void appendToken() {
		tokenBuilder.append(c);
	}
	
	/**
	 * Finish the current token being matched an add it to the token list
	 * @param t The token type
	 */
	private void finishToken(EnumToken t) {
		Token token = new Token(t, tokenBuilder.toString(), tokenRow, tokenCol);
		printTokens.push(token);
		
		if (token.getEnum() != EnumToken.whitespace) tokens.push(token);		
		
		// Reset for next token
		tokenBuilder.setLength(0);
		tokenCol = col;
		tokenRow = row;
	}
	
	/**
	 * Read the next character from the stream in `c`. Updates `row` and `col`
	 * @throws IOException
	 */
	private void read() throws IOException {
		c = (char) stream.read();
		
		if (c == '\n') {
			row += 1;
			col = 0;
		} else {
			col += 1;
		}
	}
	
	/**
	 * @return True if reached end of stream
	 */
	private boolean done() {
		return ((short) c) == -1;
	}
	
	@Override
	public boolean hasNext() {
		return !done() || tokenIdx != tokens.size();
	}
	
	/**
	 * Lex the next token from the stream or null if reached end of stream
	 * @throws LexExcpetion If failed to lex
	 */
	@Override
	public Token next() {
		if (tokenIdx >= tokens.size()) readToken();
		
		if (tokenIdx < tokens.size()) return tokens.get(tokenIdx++);
		else return null;
	}
	
	public Token lookAhead(int amnt) {
		while (tokenIdx + amnt >= tokens.size()) readToken();
		
		return tokens.get(tokenIdx + amnt);
	}
	
	private void readToken() {
		try {
			while (!done()) {
				if (c == '#') {
					handleComment();
					tokenIdx += 1;
					continue;
				} else if (isWhitespace(c)) {
	                handleWhitespace();
	                continue;
				} else if (c == '$') {
	                handleBit();
	                break;
				} else if (isSeparator(c)) {
					handleSeparator();
					break;
				} else if (isOperator(c)) {
	                handleGroup(operators, EnumToken.Group.OPERATOR);
	                break;
	            } else if (isIdentifier(c)) {
	                handleGroup(identifier, EnumToken.identifier);
	                break;
	            } else {
	            	tokenBuilder.append(c);
	                throw new LexException("Unknown token \"" + tokenBuilder.toString() + "\"", row, col);
	            }
			}
		} catch (IOException e) {
			throw new LexException("Failed to read input:\n" + e, row, col);
		}
	}
	
	/**
	 * Parses a comment. When returns index points to next non-comment character
	 * @throws IOException
	 */
	private void handleComment() throws IOException {
		do {
			appendToken(); // Add matched char
			read(); // Load next char
		} while (!done() && c != '#' && c != '\n');
		
		// Handle based on ending token
		if (done()) {
			finishToken(EnumToken.comment);
		} else if (c == '#') {
			appendToken();
			finishToken(EnumToken.comment);
			read(); // read must be last after appendToken
		} else if (c == '\n') {
			finishToken(EnumToken.comment);
		} else {
			throw new RuntimeException();
		}
	}
	
	/**
	 * Parses whitespace. When returns index points to next non-whitespace character
	 * @throws IOException
	 */
	private void handleWhitespace() throws IOException {
		do {
			appendToken();
			read();
		} while (!done() && isWhitespace(c));
		
		finishToken(EnumToken.whitespace);
	}
	
	/**
	 * Parse a bit constant. WHen returns index points to next non-bit character
	 * @throws IOException
	 */
	private void handleBit() throws IOException {
		appendToken();
		read();
		
		// Only valid bits
		if (c == '0' || c == '1') {
			appendToken();
			read();
			
			finishToken(EnumToken.bit);
		} else if (done()) {
			throw new LexException("Unexpected end of file", row, col);
		} else {
			throw new LexException("Invalid bit '$" + c + "'", tokenRow, tokenCol); 
		}
	}
	
	/**
	 * Parses a single-character separator. When returns index points to the next character
	 * @throws IOException
	 */
	private void handleSeparator() throws IOException {
		appendToken();
		read();
		
		finishToken(EnumToken.valueOf(tokenBuilder.toString(), EnumToken.Group.SEPERATOR));
	}
	
	/**
	 * Handle a group
	 * @param continuers The characters that continue the group
	 * @param matchGroup EnumTokens that the group can match
	 * @throws IOException
	 */
	private void handleGroup(char[] continuers, List<EnumToken> matchGroup) throws IOException {
		handleGroup(continuers, matchGroup, null);
	}
	
	/**
	 * Handle a group
	 * @param continuers The characters that continue the group
	 * @param t The EnumToken that the group is assigned
	 * @throws IOException
	 */
	private void handleGroup(char[] continuers, EnumToken t) throws IOException {
		handleGroup(continuers, null, t);
	}
	
	/**
	 * Handle a group
	 * @param continuers The characters that continue the group
	 * @param matchGroup The EnumTokens that the group can match if `t` is null
	 * @param t The EnumToken to assign the group, or null if has to match an enum in the `matchGroup` list
	 * @throws IOException
	 */
	private void handleGroup(char[] continuers, List<EnumToken> matchGroup, EnumToken t) throws IOException {
		do {
			appendToken();
			read();
		} while (!done() && Arrays.binarySearch(continuers, c) >= 0);
		
		String strToken = tokenBuilder.toString();
		if (t == null) t = EnumToken.valueOf(strToken, matchGroup);
		
		if (t == null) throw new LexException("Invalid token '" + strToken + "'", tokenRow, tokenCol);
		else finishToken(t);
	}
	
	private boolean isIdentifier(char c) {
		return Arrays.binarySearch(identifier, c) >= 0;
	}
	
	private boolean isOperator(char c) {
		return Arrays.binarySearch(operators, c) >= 0;
	}
	
	private boolean isSeparator(char c) {
		return Arrays.binarySearch(separators, c) >= 0;
	}
	
	private boolean isWhitespace(char c) {
		return Arrays.binarySearch(whitespace, c) >= 0;
	}
	
	static final char[] identifier = "_@%^*-+.<>?/\\|{}:'\"~`abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();
    static final char[] operators = "&|!=".toCharArray();
    static final char[] separators = ",[]();".toCharArray();
    static final char[] whitespace = " \t\n\f\r".toCharArray();
    static {
        Arrays.sort(identifier);
        Arrays.sort(operators);
        Arrays.sort(separators);
        Arrays.sort(whitespace);
    }	
}
