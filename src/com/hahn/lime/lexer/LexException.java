package com.hahn.lime.lexer;

public class LexException extends RuntimeException {
	private static final long serialVersionUID = -4562805721621949550L;

	public LexException(String mss, int row, int col) {
		super(mss + " at " + row + "," + col);
	}
	
}
