package com.hahn.lime.exceptions;

import com.hahn.lime.lexer.Token;

public class DuplicateInfixDefinitionException extends RuntimeException {
	private static final long serialVersionUID = 7269713586139964715L;
	
	public DuplicateInfixDefinitionException(Token name) {
		super("An prefix expression \"" + name + "\" has already been defined at " + name.getRow() + "," + name.getCol());
	}

}
