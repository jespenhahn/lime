package com.hahn.lime.exceptions;

import com.hahn.lime.parser.expressions.NameExpression;

public class DuplicateFunctionDefinitionException extends RuntimeException {
	private static final long serialVersionUID = -1281360138019917910L;

	public DuplicateFunctionDefinitionException(String mss, NameExpression name) {
		super(mss + " at " + name.getRow() + "," + name.getCol());
	}
	
}
