package com.hahn.lime.exceptions;

import com.hahn.lime.lexer.Token;

public class UndefinedInfixException extends RuntimeException {
	private static final long serialVersionUID = -441360860526556645L;
	
	public UndefinedInfixException(Token name) {
		super("Undefined infix expression \"" + name + "\" at " + name.getRow() + "," + name.getCol());
	}

}
