package com.hahn.lime.exceptions;

import com.hahn.lime.lexer.Locatable;

public class MismatchArgumentException extends RuntimeException {
	private static final long serialVersionUID = 6888931402470780905L;

	public MismatchArgumentException(String mss, Locatable l) {
		super(mss + " at " + l.getRow() + "," + l.getCol());
	}
	
}
