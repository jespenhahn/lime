package com.hahn.lime.exceptions;

import com.hahn.lime.parser.expressions.NameExpression;

public class DuplicateVariableDefinitionException extends RuntimeException {
	private static final long serialVersionUID = -2609749827052118761L;

	public DuplicateVariableDefinitionException(String mss, NameExpression name) {
		super(mss + " at " + name.getRow() + "," + name.getCol());
	}
	
}
