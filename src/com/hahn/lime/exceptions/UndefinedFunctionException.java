package com.hahn.lime.exceptions;

import com.hahn.lime.parser.expressions.NameExpression;

public class UndefinedFunctionException extends RuntimeException {
	private static final long serialVersionUID = 8556203022847734599L;
	
	public UndefinedFunctionException(String mss, NameExpression name) {
		super(mss + " at " + name.getRow() + "," + name.getCol());
	}

}
