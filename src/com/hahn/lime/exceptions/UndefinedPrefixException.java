package com.hahn.lime.exceptions;

import com.hahn.lime.lexer.Token;

public class UndefinedPrefixException extends RuntimeException {
	private static final long serialVersionUID = -441360860526556645L;
	
	public UndefinedPrefixException(Token name) {
		super("Undefined prefix expression \"" + name + "\" at " + name.getRow() + "," + name.getCol());
	}

}
