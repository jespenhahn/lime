package com.hahn.lime.exceptions;

import com.hahn.lime.lexer.Token;

public class ParseException extends RuntimeException {
	private static final long serialVersionUID = -8777756061698719053L;
	
	private Token token;
	
	public ParseException(String mss, Token next) {
		super(mss + " at " + next.getRow() + "," + next.getCol());
		this.token = next;
	}
	
}
