package com.hahn.lime.exceptions;

import com.hahn.lime.parser.expressions.NameExpression;

public class UndefinedVariableException extends RuntimeException {
	private static final long serialVersionUID = -6445337688948144681L;

	public UndefinedVariableException(String mss, NameExpression name) {
		super(mss + " at " + name.getRow() + "," + name.getCol());
	}
	
}
