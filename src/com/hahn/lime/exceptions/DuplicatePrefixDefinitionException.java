package com.hahn.lime.exceptions;

import com.hahn.lime.lexer.Token;

public class DuplicatePrefixDefinitionException extends RuntimeException {
	private static final long serialVersionUID = 7269713586139964715L;
	
	public DuplicatePrefixDefinitionException(Token name) {
		super("An infix expression \"" + name + "\" has already been defined at " + name.getRow() + "," + name.getCol());
	}

}
