package com.hahn.lime.util;

import java.util.Collection;

public class StringUtils {

	public static String join(Collection<?> cs, String d) {
		boolean first = true;
		StringBuilder builder = new StringBuilder();
		for (Object c: cs) {
			if (!first) builder.append(d);
			else first = false;
			
			builder.append(c.toString());
		}
		
		return builder.toString();
	}
	
}
