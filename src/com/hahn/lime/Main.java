package com.hahn.lime;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Stack;

import com.hahn.lime.interpreter.Interpreter;
import com.hahn.lime.interpreter.custom.AssignInterpreter;
import com.hahn.lime.interpreter.custom.GroupInterpreter;
import com.hahn.lime.interpreter.custom.InfixAndInterpreter;
import com.hahn.lime.interpreter.custom.InfixBorInterpreter;
import com.hahn.lime.interpreter.custom.Interpreters;
import com.hahn.lime.interpreter.custom.PrefixNotInterpreter;
import com.hahn.lime.lexer.EnumToken;
import com.hahn.lime.parser.expressions.Expression;
import com.hahn.lime.parser.parselets.AndOperatorParselet;
import com.hahn.lime.parser.parselets.AssignParselet;
import com.hahn.lime.parser.parselets.BitParselet;
import com.hahn.lime.parser.parselets.BorOperatorParselet;
import com.hahn.lime.parser.parselets.CallParselet;
import com.hahn.lime.parser.parselets.DefFuncParselet;
import com.hahn.lime.parser.parselets.GroupParselet;
import com.hahn.lime.parser.parselets.MatrixParselet;
import com.hahn.lime.parser.parselets.NameParselet;
import com.hahn.lime.parser.parselets.Parselets;
import com.hahn.lime.parser.parselets.PrefixOperatorParselet;
import com.hahn.lime.util.StringUtils;

public class Main {
	
	public static void main(String[] args) throws IOException {
		defineSyntax();
		
		Collection<Expression> es = interpret(new FileInputStream(new File("code.l")));
		System.out.println(StringUtils.join(es, "\n"));
	}
	
	private static void defineSyntax() {		
		// Prefix with interpreter
		Parselets.registerPrefix(EnumToken.bit, new BitParselet());
		Parselets.registerPrefix(EnumToken.identifier, new NameParselet());
		Parselets.registerPrefix(EnumToken.open_s, new MatrixParselet());
		
		// Prefix with interpreter
		Parselets.registerPrefix(EnumToken.not, new PrefixOperatorParselet());
		Interpreters.registerPrefix(EnumToken.not, new PrefixNotInterpreter());
		
		Parselets.registerPrefix(EnumToken.open_p, new GroupParselet());
		Interpreters.registerPrefix(EnumToken.open_p, new GroupInterpreter());
		
		
		// Infix without interpreter
		Parselets.registerInfx(EnumToken.identifier, new DefFuncParselet());
		Parselets.registerInfx(EnumToken.open_p, new CallParselet());
		
		// Infix with interpreter
		Parselets.registerInfx(EnumToken.equ, new AssignParselet());
		Interpreters.registerInfix(EnumToken.equ, new AssignInterpreter());
		
		Parselets.registerInfx(EnumToken.and, new AndOperatorParselet());
		Interpreters.registerInfix(EnumToken.and, new InfixAndInterpreter());
		
		Parselets.registerInfx(EnumToken.bor, new BorOperatorParselet());
		Interpreters.registerInfix(EnumToken.bor, new InfixBorInterpreter());		
	}
	
	/**
	 * Parse the given stream. O(n)
	 * @param is The stream to parse
	 * @return A stack of the parsed stream
	 */
	private static Stack<Expression> interpret(InputStream is) {
		try {
			Interpreter in = new Interpreter();
			return in.interpret(is);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}

}
