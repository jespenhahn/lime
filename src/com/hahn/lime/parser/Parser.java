package com.hahn.lime.parser;

import com.hahn.lime.exceptions.ParseException;
import com.hahn.lime.lexer.EnumToken;
import com.hahn.lime.lexer.Lexer;
import com.hahn.lime.lexer.Token;
import com.hahn.lime.parser.expressions.Expression;
import com.hahn.lime.parser.parselets.InfixParselet;
import com.hahn.lime.parser.parselets.Parselets;
import com.hahn.lime.parser.parselets.PrefixParselet;

public class Parser {
	Lexer lexer;

	public Parser(Lexer lexer) {
		this.lexer = lexer;
	}

	public Expression parseExpression() {
		return parseExpression(0, false);
	}
	
	public Expression parseExpression(final int precedence, final boolean allowSemicolon) {
		Token token = lexer.next();
		if (token == null || token.getEnum() == EnumToken.semicolon) {
			if (allowSemicolon) return null;
			else throw new ParseException("Could not parse \"" + token.getVal() + "\"", token);
		}
		
		PrefixParselet prefix = Parselets.getPrefix(token);
		if (prefix == null) throw new ParseException("Could not parse \"" + token.getVal() + "\"", token);

		Expression left = prefix.parse(this, token);
		while (lexer.hasNext() && precedence < getPrecedence()) {
			token = lexer.next();
			
			InfixParselet infix = Parselets.getInfix(token);
			if (infix == null) throw new ParseException("Could not parse \"" + token.getVal() + "\"", token);
			
			left = infix.parse(this, left, token);
		}

		return left;
	}
	
	public Token consume(EnumToken token) {
		Token next = lexer.next();
		if (next.getEnum() != token) throw new ParseException("Missing '" + token.str + "'", next);
		
		return next;
	}
	
	public Token lookAhead() {
		return lexer.lookAhead(0);
	}

	private int getPrecedence() {
		if (lexer.hasNext()) {
			InfixParselet parser = Parselets.getInfix(lookAhead());
			if (parser != null) return parser.getPrecedence();
		}

		return 0;
	}
}
