package com.hahn.lime.parser.expressions;

import com.hahn.lime.interpreter.Interpreter;
import com.hahn.lime.interpreter.custom.Interpreters;
import com.hahn.lime.interpreter.object.Evaluated;
import com.hahn.lime.lexer.Token;

public class InfixOperatorExpression extends Expression {
	Expression left, right;
	Token opcode;
	
	public InfixOperatorExpression(Expression left, Token opcode, Expression right) {
		super(opcode);
		
		this.left = left;
		this.right = right;
		this.opcode = opcode;
	}

	@Override
	public Evaluated evaluate(Interpreter in) {
		return Interpreters.evaluateInfix(in, opcode, left, right);
	}
	
	@Override
	public String toString() {	
		return String.format("(%s %s %s)", left.toString(), opcode.toString(), right.toString());
	}
}
