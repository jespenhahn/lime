package com.hahn.lime.parser.expressions;

import java.util.Arrays;
import java.util.Collection;

import com.hahn.lime.interpreter.Interpreter;
import com.hahn.lime.interpreter.object.Evaluated;
import com.hahn.lime.interpreter.object.Matrix;
import com.hahn.lime.lexer.Token;

public class MatrixExpression extends Expression {
	private Expression[] exprs;
	
	public MatrixExpression(Token start, Collection<Expression> exprs) {
		super(start);
		
		this.exprs = exprs.toArray(new Expression[exprs.size()]);
	}
	
	@Override
	public Evaluated evaluate(Interpreter in) {
		Evaluated[] vals = new Evaluated[exprs.length];
		for (int i = 0; i < exprs.length; i++) vals[i] = exprs[i].evaluate(in);
		
		return new Matrix(vals);
	}
	
	@Override
	public Evaluated define(Interpreter in) {
		Evaluated[] vals = new Evaluated[exprs.length];
		for (int i = 0; i < exprs.length; i++) vals[i] = exprs[i].define(in);
		
		return new Matrix(vals);
	}

	@Override
	public String toString() {
		return Arrays.toString(exprs);
	}
}
