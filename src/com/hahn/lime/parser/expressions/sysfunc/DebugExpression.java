package com.hahn.lime.parser.expressions.sysfunc;

import com.hahn.lime.interpreter.Interpreter;
import com.hahn.lime.interpreter.object.Bit;
import com.hahn.lime.interpreter.object.Evaluated;
import com.hahn.lime.lexer.Locatable;
import com.hahn.lime.parser.expressions.Expression;

public class DebugExpression extends Expression {

	public DebugExpression(Locatable l) {
		super(l);
	}

	@Override
	public Evaluated evaluate(Interpreter in) {
		System.out.println(in.getLocalVar("mss").bitValue());
		
		return Bit.valueOf(0);
	}

}
