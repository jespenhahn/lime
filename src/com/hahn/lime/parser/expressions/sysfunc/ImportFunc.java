package com.hahn.lime.parser.expressions.sysfunc;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import com.hahn.lime.interpreter.Interpreter;
import com.hahn.lime.interpreter.object.Bit;
import com.hahn.lime.interpreter.object.Evaluated;
import com.hahn.lime.interpreter.object.Func;
import com.hahn.lime.parser.expressions.Expression;
import com.hahn.lime.parser.expressions.NameExpression;

public class ImportFunc extends Func {
	private static boolean defined = false;
	
	public ImportFunc() {
		super("import", new String[] { "file" });
		
		if (!defined) defined = true;
		else throw new RuntimeException("Tried to define inport function twice!");
	}
	
	@Override
	public Evaluated call(NameExpression name, Expression[] params, Interpreter in) {
		String file = params[0].toString();
		try {
			in.interpret(new FileInputStream(new File(file)));
		} catch (IOException e) {
			throw new ImportException("Failed to import \"" + file + "\"", name);
		}
		
		return Bit.valueOf(0);
	}

	class ImportException extends RuntimeException {
		private static final long serialVersionUID = -5345417543244325000L;

		public ImportException(String mss, Expression e) {
			super(mss + " at " + e.getRow() + "," + e.getCol());
		}
		
	}
}
