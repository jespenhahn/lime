package com.hahn.lime.parser.expressions;

import com.hahn.lime.exceptions.MismatchArgumentException;
import com.hahn.lime.interpreter.Interpreter;
import com.hahn.lime.interpreter.object.Evaluated;
import com.hahn.lime.lexer.Locatable;

public abstract class Expression implements Locatable {
	private int row, col;
	
	public Expression(Locatable l) {
		if (l == null) throw new IllegalArgumentException();
		
		this.row = l.getRow();
		this.col = l.getCol();
	}
	
	@Override
	public int getRow() {
		return row;
	}
	
	@Override
	public int getCol() {
		return col;
	}
	
	public abstract Evaluated evaluate(Interpreter in);
	
	public Evaluated define(Interpreter in) {
		throw new MismatchArgumentException("Illegal left-hand argument \"" + this + "\"", this);
	}
	
}
