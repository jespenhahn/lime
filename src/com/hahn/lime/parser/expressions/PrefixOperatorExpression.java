package com.hahn.lime.parser.expressions;

import com.hahn.lime.interpreter.Interpreter;
import com.hahn.lime.interpreter.custom.Interpreters;
import com.hahn.lime.interpreter.object.Evaluated;
import com.hahn.lime.lexer.Token;

public class PrefixOperatorExpression extends Expression {
	Token opcode;
	Expression operand;	
	
	public PrefixOperatorExpression(Token opcode, Expression operand) {
		super(opcode);
		
		this.opcode = opcode;
		this.operand = operand;
	}
	
	@Override
	public Evaluated evaluate(Interpreter in) {
		return Interpreters.evaluatePrefix(in, opcode, operand);
	}
	
	@Override
	public String toString() {
		return String.format("(%s %s)", opcode.toString(), operand.toString());
	}
}
