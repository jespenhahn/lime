package com.hahn.lime.parser.expressions;

import com.hahn.lime.interpreter.Interpreter;
import com.hahn.lime.interpreter.object.Bit;
import com.hahn.lime.lexer.Token;

public class BitExpression extends Expression {
	private Bit bit;
	
	public BitExpression(Token bitToken) {
		super(bitToken);
		
		this.bit = Bit.valueOf(Integer.valueOf(bitToken.getVal().substring(1))); 
	}
	
	@Override
	public Bit evaluate(Interpreter in) {
		return bit;
	}
	
	@Override
	public String toString() {
		return String.valueOf(bit);
	}
}
