package com.hahn.lime.parser.expressions;

import com.hahn.lime.interpreter.Interpreter;
import com.hahn.lime.interpreter.object.Evaluated;
import com.hahn.lime.lexer.Token;

public class NameExpression extends Expression {
	String name;
	
	public NameExpression(Token name) {
		super(name);
		
		this.name = name.getVal();
	}
	
	public String getName() {
		return name;
	}
	
	@Override
	public Evaluated evaluate(Interpreter in) {
		return in.getVar(this);
	}
	
	@Override
	public Evaluated define(Interpreter in) {
		return in.defVar(this);
	}
	
	@Override
	public String toString() {
		return getName();
	}
}
