package com.hahn.lime.parser.expressions;

import java.util.Arrays;
import java.util.List;

import com.hahn.lime.exceptions.MismatchArgumentException;
import com.hahn.lime.interpreter.Interpreter;
import com.hahn.lime.interpreter.object.Evaluated;

public class FuncExpression extends Expression {
	private NameExpression name;
	private Expression[] params;
	
	public FuncExpression(NameExpression name, List<Expression> params) {
		super(name);
		
		this.name = name;
		this.params = params.toArray(new Expression[params.size()]);
	}
	
	@Override
	public Evaluated evaluate(Interpreter in) {
		return in.getFunc(name, params.length).call(name, params, in);
	}
	
	@Override
	public Evaluated define(Interpreter in) {
		String[] pNames = new String[params.length];
		for (int i = 0; i < params.length; i++) {
			Expression pExpr = params[i];
			if (pExpr instanceof NameExpression) pNames[i] = ((NameExpression) pExpr).getName();
			else throw new MismatchArgumentException("Cannot define a function's parameter based on an expression", this);
		}
		
		return in.defFunc(name, pNames);
	}
	
	@Override
	public String toString() {
		return String.format("(%s %s)", name, Arrays.toString(params));
	}
}