package com.hahn.lime.parser.parselets;

import com.hahn.lime.parser.Precedence;

public class BorOperatorParselet extends BinaryOperatorParselet {

	@Override
	public int getPrecedence() {
		return Precedence.BOR;
	}

}
