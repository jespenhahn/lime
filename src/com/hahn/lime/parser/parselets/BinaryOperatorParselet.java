package com.hahn.lime.parser.parselets;

import com.hahn.lime.lexer.Token;
import com.hahn.lime.parser.Parser;
import com.hahn.lime.parser.expressions.Expression;
import com.hahn.lime.parser.expressions.InfixOperatorExpression;

public abstract class BinaryOperatorParselet implements InfixParselet {
	
	@Override
	public Expression parse(Parser parser, Expression left, Token token) {
		Expression right = parser.parseExpression(getPrecedence(), false);
		return new InfixOperatorExpression(left, token, right);
	}
}
