package com.hahn.lime.parser.parselets;

import com.hahn.lime.lexer.Token;
import com.hahn.lime.parser.Parser;
import com.hahn.lime.parser.Precedence;
import com.hahn.lime.parser.expressions.Expression;
import com.hahn.lime.parser.expressions.PrefixOperatorExpression;

public class PrefixOperatorParselet implements PrefixParselet {

	@Override
	public Expression parse(Parser parser, Token token) {
		Expression operand = parser.parseExpression(getPrecedence(), false);
		return new PrefixOperatorExpression(token, operand);
	}
	
	public int getPrecedence() {
		return Precedence.PREFIX;
	}
	
}