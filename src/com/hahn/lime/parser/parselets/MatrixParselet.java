package com.hahn.lime.parser.parselets;

import java.util.Stack;

import com.hahn.lime.lexer.EnumToken;
import com.hahn.lime.lexer.Token;
import com.hahn.lime.parser.Parser;
import com.hahn.lime.parser.expressions.Expression;
import com.hahn.lime.parser.expressions.MatrixExpression;

public class MatrixParselet implements PrefixParselet {

	@Override
	public Expression parse(Parser parser, Token token) {		
		Stack<Expression> exps = new Stack<Expression>();
		while (parser.lookAhead().getEnum() != EnumToken.close_s) {
			exps.push(parser.parseExpression());
			
			if (parser.lookAhead().getEnum() != EnumToken.close_s) {
				parser.consume(EnumToken.comma);
			}
		}
		
		parser.consume(EnumToken.close_s);
		
		return new MatrixExpression(token, exps);
	}
	
}
