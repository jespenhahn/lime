package com.hahn.lime.parser.parselets;

import com.hahn.lime.lexer.Token;
import com.hahn.lime.parser.Parser;
import com.hahn.lime.parser.expressions.Expression;

public interface InfixParselet {
	Expression parse(Parser parser, Expression left, Token token);
	
	/**
	 * Fallback precedence if not defined in Precedence mapping
	 * @return Precedence that can be overridden by adding entry to Precedence mapping
	 */
	int getPrecedence();
}