package com.hahn.lime.parser.parselets;

import java.util.Stack;

import com.hahn.lime.exceptions.ParseException;
import com.hahn.lime.lexer.EnumToken;
import com.hahn.lime.lexer.Token;
import com.hahn.lime.parser.Parser;
import com.hahn.lime.parser.Precedence;
import com.hahn.lime.parser.expressions.FuncExpression;
import com.hahn.lime.parser.expressions.Expression;
import com.hahn.lime.parser.expressions.NameExpression;

public class CallParselet implements PostfixParselet {

	@Override
	public Expression parse(Parser parser, Expression left, Token token) {
		NameExpression name;
		if (left instanceof NameExpression) name = (NameExpression) left;
		else throw new ParseException("Could not parse \"" + token.getVal() + "\"", token);
		
		Stack<Expression> params = new Stack<Expression>();
		while (parser.lookAhead().getEnum() != EnumToken.close_p) {
			params.push(parser.parseExpression());
			
			if (parser.lookAhead().getEnum() != EnumToken.comma) {
				parser.consume(EnumToken.close_p);
				break;
			} else {
				parser.consume(EnumToken.comma);
			}
		}
		
		return new FuncExpression(name, params);
	}
	
	@Override
	public int getPrecedence() {
		return Precedence.CALL;
	}

}
