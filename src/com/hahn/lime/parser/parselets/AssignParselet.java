package com.hahn.lime.parser.parselets;

import com.hahn.lime.parser.Precedence;

public class AssignParselet extends BinaryOperatorParselet {

	@Override
	public int getPrecedence() {
		return Precedence.ASSIGN;
	}

}
