package com.hahn.lime.parser.parselets;

import com.hahn.lime.lexer.Token;
import com.hahn.lime.parser.Parser;
import com.hahn.lime.parser.expressions.BitExpression;
import com.hahn.lime.parser.expressions.Expression;

public class BitParselet implements PrefixParselet {
	
	@Override
	public Expression parse(Parser parser, Token token) {
		return new BitExpression(token);
	}
	
}
