package com.hahn.lime.parser.parselets;

import com.hahn.lime.lexer.Token;
import com.hahn.lime.parser.Parser;
import com.hahn.lime.parser.expressions.Expression;

public class PostfixOperatorParselet implements InfixParselet {
	
	public Expression parse(Parser parser, Expression left, Token token) {
		return left;
	}

	@Override
	public int getPrecedence() {
		return 0;
	}
	
}
