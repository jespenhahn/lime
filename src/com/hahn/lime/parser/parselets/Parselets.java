package com.hahn.lime.parser.parselets;

import java.util.HashMap;
import java.util.Map;

import com.hahn.lime.lexer.EnumToken;
import com.hahn.lime.lexer.Token;

public class Parselets {
	public static void registerPrefix(EnumToken token, PrefixParselet parselet) {
		registerPrefix(token.str, parselet);
	}
	
	public static void registerPrefix(String token, PrefixParselet parselet) {
		mPrefixParselets.put(token, parselet);
	}
	
	public static PrefixParselet getPrefix(Token token) {
		PrefixParselet p = mPrefixParselets.get(token.getVal());
		if (p == null) p = mPrefixParselets.get(token.getEnum().str); // throw new UndefinedPrefixException(token);
		
		return p;
	}
	
	public static void registerInfx(EnumToken token, InfixParselet parselet) {
		registerInfx(token.str, parselet);
	}
	
	public static void registerInfx(String token, InfixParselet parselet) {
		mInfixParselets.put(token, parselet);
	}
	
	public static InfixParselet getInfix(Token token) {
		InfixParselet p = mInfixParselets.get(token.getVal());
		if (p == null) p = mInfixParselets.get(token.getEnum().str); // throw new UndefinedInfixException(token);
		
		return p;
	}
	
	private static final Map<String, PrefixParselet> mPrefixParselets = new HashMap<String, PrefixParselet>();
	private static final Map<String, InfixParselet> mInfixParselets = new HashMap<String, InfixParselet>();
}
