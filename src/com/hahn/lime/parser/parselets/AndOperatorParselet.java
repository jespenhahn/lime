package com.hahn.lime.parser.parselets;

import com.hahn.lime.parser.Precedence;

public class AndOperatorParselet extends BinaryOperatorParselet {

	@Override
	public int getPrecedence() {
		return Precedence.AND;
	}

	

}
