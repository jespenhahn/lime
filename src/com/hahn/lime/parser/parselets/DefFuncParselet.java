package com.hahn.lime.parser.parselets;

import java.util.ArrayList;
import java.util.List;

import com.hahn.lime.exceptions.ParseException;
import com.hahn.lime.lexer.EnumToken;
import com.hahn.lime.lexer.Token;
import com.hahn.lime.parser.Parser;
import com.hahn.lime.parser.Precedence;
import com.hahn.lime.parser.expressions.Expression;
import com.hahn.lime.parser.expressions.FuncExpression;
import com.hahn.lime.parser.expressions.NameExpression;

public class DefFuncParselet implements PostfixParselet {

	@Override
	public Expression parse(Parser parser, Expression left, Token token) {
		NameExpression name;
		if (left instanceof NameExpression) name = (NameExpression) left;
		else throw new ParseException("Could not parse \"" + token.getVal() + "\"", token);
		
		List<Expression> params = new ArrayList<Expression>();		
		params.add(new NameExpression(token)); 
		
		while (parser.lookAhead().getEnum() == EnumToken.comma) {
			parser.consume(EnumToken.comma);
			
			params.add(new NameExpression(parser.consume(EnumToken.identifier)));
		}
		
		return new FuncExpression(name, params);
	}
	
	@Override
	public int getPrecedence() {
		return Precedence.CALL;
	}

}
