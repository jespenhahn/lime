package com.hahn.lime.parser.parselets;

import com.hahn.lime.lexer.Token;
import com.hahn.lime.parser.Parser;
import com.hahn.lime.parser.expressions.Expression;

public interface PrefixParselet {
	Expression parse(Parser parser, Token token);
}
