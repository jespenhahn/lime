package com.hahn.lime.parser.parselets;

import com.hahn.lime.lexer.Token;
import com.hahn.lime.parser.Parser;
import com.hahn.lime.parser.expressions.Expression;
import com.hahn.lime.parser.expressions.NameExpression;

public class NameParselet implements PrefixParselet {
	
	public Expression parse(Parser parser, Token token) {
		return new NameExpression(token);
	}
	
}
