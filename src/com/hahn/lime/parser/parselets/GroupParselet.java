package com.hahn.lime.parser.parselets;

import com.hahn.lime.lexer.EnumToken;
import com.hahn.lime.lexer.Token;
import com.hahn.lime.parser.Parser;
import com.hahn.lime.parser.expressions.Expression;

public class GroupParselet implements PrefixParselet {

	@Override
	public Expression parse(Parser parser, Token token) {
		Expression rs = parser.parseExpression();
		parser.consume(EnumToken.close_p);		
		return rs;
	}
	
}
