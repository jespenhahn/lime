XOR p, q = (p & !q) | (!p & q);
ADD a, b, c = [ XOR(XOR(a,b),c) , (XOR(a,b) & c) | a & b ];